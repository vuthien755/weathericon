$(document).ready(function(){
    // // Api url
    let url = "http://api.openweathermap.org/data/2.5/forecast?q=Ha%20Noi,"
    + "Viet%20Nam&appid=88b432540df813d3c8baffbd1021bed4&lang=vi&units=metric";

    // Mảng chuyển dữ liệu api sang thứ trong tuần tiếng việt
    const weekdays = {
        0: "Chủ nhật",
        1: "Thứ hai",
        2: "Thứ ba",
        3: "Thứ tư",
        4: "Thứ năm",
        5: "Thứ sáu",
        6: "Thứ bảy"
    };

    // Mảng chuyển đổi điều kiện thời tiết => ảnh động tương ứng
    const weatherCondition = {
        Thunderstorm: 11,
        Rain: 10,
        Drizzle: 9,
        Clouds: 4,
        Clear: 1,
        Snow: 13,
        Fog: 15,
    };

    // Mảng chuyển đổi điều kiện thời tiết => icon của dự báo thời tiết
    const weatherIcon = {
        Thunderstorm: "wi-thunderstorm",
        Rain: "wi-rain",
        Drizzle: "wi-sprinkle",
        Clouds: "wi-cloudy",
        Clear: "wi-day-sunny",
        Snow: "wi-snow",
        Fog: "wi-fog"
    }

    // Đối tượng lưu trữ thông tin đọc từ api
    dataApi = [];

    // Mảng các tỉnh thành Việt Nam và tọa độ địa lý
    let provinceLatLog = [
        { name: 'An Giang', lat: '10.515', log: '105.113'},
        { name: 'Bà Rịa - Vũng Tàu', lat: '10.582', log: '107.290'},
        { name: 'Bắc Giang', lat: '21.309', log: '106.617'},
        { name: 'Bắc Kạn', lat: '22.257', log: '105.859'},
        { name: 'Bạc Liêu', lat: '9.270', log: '105.760'},
        { name: 'Bắc Ninh', lat: '21.184', log: '106.072'},
        { name: 'Bến Tre', lat: '10.245', log: '106.373'},
        { name: 'Bình Định', lat: '16.353', log: '107.483'},
        { name: 'Bình Dương', lat: '11.217', log: '106.658'},
        { name: 'Bình Phước', lat: '11.679', log: '106.798'},
        { name: 'Bình Thuận', lat: '11.101', log: '107.942'},
        { name: 'Cà Mau', lat: '9.179', log: '105.146'},
        { name: 'Cao Bằng', lat: '22.667', log: '106.259'},
        { name: 'Đắk Lắk', lat: '12.825', log: '108.208'},
        { name: 'Đắk Nông', lat: '12.229', log: '107.689'},
        { name: 'Điện Biên', lat: '21.711', log: '103.023'},
        { name: 'Đồng Nai', lat: '11.006', log: '107.192'},
        { name: 'Đồng Tháp', lat: '10.565', log: '105.607'},
        { name: 'Gia Lai', lat: '13.796', log: '108.261'},
        { name: 'Hà Giang', lat: '22.819', log: '104.966'},
        { name: 'Hà Nam', lat: '20.527', log: '105.954'},
        { name: 'Hà Tĩnh', lat: '18.345', log: '105.902'},
        { name: 'Hải Dương', lat: '20.943', log: '106.323'},
        { name: 'Hậu Giang', lat: '9.784', log: '105.624'},
        { name: 'Hòa Bình', lat: '9.287', log: '105.627'},
        { name: 'Hưng Yên', lat: '20.657', log: '106.062'},
        { name: 'Khánh Hòa', lat: '12.196', log: '108.995'},
        { name: 'Kiên Giang', lat: '9.762', log: '104.324'},
        { name: 'Kon Tum', lat: '14.362', log: '108.004'},
        { name: 'Lai Châu', lat: '22.400', log: '103.452'},
        { name: 'Lâm Đồng', lat: '11.751', log: '108.096'},
        { name: 'Lạng Sơn', lat: '21.853', log: '106.760'},
        { name: 'Lào Cai', lat: '22.500', log: '103.966'},
        { name: 'Long An', lat: '10.481', log: '107.201'},
        { name: 'Nam Định', lat: '20.423', log: '106.169'},
        { name: 'Nghệ An', lat: '19.223', log: '105.001'},
        { name: 'Ninh Bình', lat: '20.255', log: '105.976'},
        { name: 'Ninh Thuận', lat: '11.745', log: '108.898'},
        { name: 'Phú Thọ', lat: '21.417', log: '105.236'},
        { name: 'Quảng Bình', lat: '17.546', log: '106.258'},
        { name: 'Quảng Nam', lat: '15.597', log: '107.976'},
        { name: 'Quảng Ngãi', lat: '15.079', log: '108.916'},
        { name: 'Quảng Ninh', lat: '21.106', log: '107.485'},
        { name: 'Quảng Trị', lat: '16.880', log: '107.090'},
        { name: 'Sóc Trăng', lat: '9.610', log: '105.985'},
        { name: 'Sơn La', lat: '21.327', log: '103.915'},
        { name: 'Tây Ninh', lat: '11.404', log: '106.162'},
        { name: 'Thái Bình', lat: '20.449', log: '106.342'},
        { name: 'Thái Nguyên', lat: '21.693', log: '105.824'},
        { name: 'Thanh Hóa', lat: '19.812', log: '105.775'},
        { name: 'Thừa Thiên Huế', lat: '16.475', log: '107.705'},
        { name: 'Tiền Giang', lat: '10.381', log: '106.235'},
        { name: 'Trà Vinh', lat: '9.936', log: '106.340'},
        { name: 'Tuyên Quang', lat: '21.788', log: '105.217'},
        { name: 'Vĩnh Long', lat: '10.254', log: '105.967'},
        { name: 'Vĩnh Phúc', lat: '21.311', log: '105.603'},
        { name: 'Yên Bái', lat: '21.696', log: '104.875'},
        { name: 'Phú Yên', lat: '13.192', log: '109.214'},
        { name: 'Cần Thơ', lat: '10.038', log: '105.781'},
        { name: 'Đà Nẵng', lat: '16.068', log: '108.212'},
        { name: 'Hải Phòng', lat: '20.699', log: '106.864'},
        { name: 'Hà Nội', lat: '21.029', log: '105.854'},
        { name: 'TP HCM', lat: '10.776', log: '106.702'},
    ];

    // Hàm khởi tạo
    function start() {
        // Gọi hàm gọi api
        getWeather();
    }
    start();

    // Hàm gọi api
    function getWeather() {
        fetch(url)
        .then(response => {
            return response.json();
        })
        .then(function(response) {

            // Reset dữ liệu của đối tượng dataApi
            dataApi = [];

            // Chèn tên thành phố
            $(".city").html(response.city.name);

            // Gọi hàm xử lý render thời tiết hiện tại
            renderCurrentHandler(response);

            // Gọi hàm xử lý render dự báo thời tiết
            renderForecastHandler(dataApi);
        })
    };

    // Hàm xử lý render thời tiết hiện tại
    function renderCurrentHandler(response) {
        for(let i = 0; i <= response.list.length; i+=8) {

            // Khai báo biến thời gian
            let dateView = new Date(response.list[i].dt_txt);

            // Khai báo biến dẫn xuất từ Api
            let data = response.list[i];
            
            if(i == 0) {

                // Hiển thị thứ trong tuần
                $(".time").html(`${weekdays[dateView.getDay()]}
                ${dateView.getDate()}-${(dateView.getMonth() + 1) < 10 ? "0" 
                + (dateView.getMonth() + 1):"error"}-${dateView.getFullYear()}`);

                // Hiển thị nhiệt độ hiện tại lúc 9h sáng
                $(".temp").html(`${Math.round(data.main.temp)}° C`);

                // Hiển thị mô tả thời tiết
                $(".description").html(data.weather[0].description);
                
                // Vẽ lại icon thời tiết hiện tại
                $("#weather-icon").empty();
                WeatherIcon.add('weather-icon', weatherCondition[data.weather[0].main],  
                {mode:WeatherIcon.DAY, stroke:false, shadow: false, animated: true});

                // Giảm i để render đủ 5 ngày tiếp theo vào khung giờ 6h sáng
                i--;
            } else {

                // Lấy dữ liệu để dùng render dự báo thời tiết
                dataApi.push({
                    date: weekdays[dateView.getDay()],
                    temp: `${Math.round(data.main.temp)}° C`,
                    icon: weatherIcon[data.weather[0].main]
                });
            }
        }
    };

    // Hàm xử lý render dự báo thời tiết
    function renderForecastHandler() {

        // Reset dự báo thời tiết
        $("#forecast").html = "";
        
        // Render dữ liệu dự báo thời tiết
        $("#forecast").html(dataApi.map(e => 
        `<li class="forecast-item">
            <div>${e.date}</div>
            <i class="wi ${e.icon}"></i>
            <div>${e.temp}</div>
        </li>`).join(""));
    };

    // Khởi tạo select2
    $('.js-example-basic-single').select2();
    $('.js-example-basic-single').append(provinceLatLog.map(e=>
        `<option>${e.name}</option>`));

    // Sự kiện lắng nghe select2
    $('.js-example-basic-single').change(function(e) {
        let name = $(".select2-selection__rendered").text();
        url = `http://api.openweathermap.org/data/2.5/forecast?&appid=88b432540df813d3c8baffbd1021bed4&lang=vi&units=metric&lat=${provinceLatLog.find(e=>e.name==name).lat}&lon=${provinceLatLog.find(e=>e.name==name).log}`;
        start();
    });
});